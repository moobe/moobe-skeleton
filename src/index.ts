require('dotenv').config();
import * as options from '@config/config.json';
import { app } from '@moobe/moobe-app';

app.bootstrap(options).run();
