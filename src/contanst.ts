import { join } from 'path';
export const PROJECT_DIR = __dirname + '/';
export const CONFIG_DIR = join(PROJECT_DIR, 'config');
export const CONFIG_FILE = join(CONFIG_DIR, 'config.json');
export const COMPONENTS_DIR = join(PROJECT_DIR, 'components');
export const CONFIG_COMPONENTS_DIR = join(CONFIG_DIR, 'components');
